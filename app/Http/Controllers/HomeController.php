<?php

namespace App\Http\Controllers;

use App\Models\Key;
use App\Models\Product;
use App\Models\Used_key;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){
        $validate = false;
        $keys = new Key;

        $product = new Product;
        if (request('product_id') ){
            $validate = true;
            $product = Product::find(request('product_id'));
            $keys = $keys->where("product_id", request('product_id'))->where('used', 0)->simplePaginate(10);
        }
        $products = Product::get();
        return view('home.index', compact('products','keys','product','validate'));
    }

    public function create(Key $key){
        $product = $key->product;
        return view('home.create' ,compact('key', 'product'));
    }

    public function store(){
        $attribute = request()->validate([
            'mac'=>'required|max:255',
            'email'=>'required|email|max:255',
            'name_sta'=>'required',
            'key_id'=>'required|unique:used_keys'
        ]);
        Used_key::create($attribute);
        Key::findOrFail($attribute['key_id'])->update(['used'=>1]);

        $contain['message'] = 'La clé numero '.$attribute['key_id'].' est maintenant utilisé par '.$attribute['email'].'.';
        $contain['type'] = "success";

        return redirect()->to('/')->with('contain', $contain);

    }
}
