<?php

namespace App\Http\Controllers;

use App\Models\Key;
use App\Models\Product;
use App\Models\Used_key;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Orchestra\Parser\Xml\Facade as XmlParser;
use function Livewire\str;


class ImportController extends Controller
{
    public function index(){
        $validate = false;
        $keys = new Key;

        $product = new Product;
        if (request('product_id') ){
            $validate = true;
            $product = Product::find(request('product_id'));
            $keys = $keys->where("product_id", request('product_id'));
        }
        $keys = $keys->get();
        $products = Product::get();
        return view('import.index', compact('products','keys','product','validate'));
    }

    public function show(Key $key){
        return view('import.show', compact('key'));
    }

    public function create(){
        $products = Product::get();

        return view('import.create', compact('products'));
    }

    public function store(){
        $contain = [];
        $attKey = request()->validate([
            'key'=>'required|max:255|unique:keys',
            'product_id'=>'required',
            'msdn'=>'required|boolean',
            'key_volume' =>'required|boolean'
        ]);
        $attKey['used'] = false;

        if (request('name_sta') || request('mac') || request('email')){
            $attUsedKey = request()->validate([
                'mac'=>'required|max:255',
                'email'=>'required|email|max:255',
                'name_sta'=>'required'
            ]);
            $attKey['used'] = true;
        }
        $key = Key::create($attKey);
        if (isset($attUsedKey)){
            $attUsedKey['key_id'] = $key->id;
            $UsedKey = Used_key::create($attUsedKey);
        }
        if ($key){

            $contain['type'] = 'success';
            $contain['message'] = "La clé numero ".$key->id." a bien été créée";
            if (isset($UsedKey)){
                $contain['message'].= " et est utilisé par ".$UsedKey->email;
            }
            $contain['message'] .= " !!";
        }else{
            $contain['type'] = 'error';
            $contain['message'] = "Erreur";

        }


        return redirect()->to('import')->with('contain', $contain);
    }

    public function storeXml(){
        $contain = [];
        $importKeys = [];
        $i = 0;
        $checker = false;
        $nameSelectedOs = strtolower(json_decode(request('os'))->name);
        $nameSelectedOs = str_replace(' ','',$nameSelectedOs);

        if (str_contains($nameSelectedOs,'windows')){
            $nameSelectedOs = str_replace('windows','',$nameSelectedOs);
        }
        if (str_contains($nameSelectedOs,'microsoft')){
            $nameSelectedOs = str_replace('microsoft','',$nameSelectedOs);
        }


        $nameFile = strtolower(request()->file("xmlFile")->getClientOriginalName());
        if (str_contains($nameFile,'keysexport_')){
            $nameFile = str_replace('keysexport_','',$nameFile);
        }
        if (str_contains($nameFile,'.xml')) {
            $nameFile = str_replace('.xml', '', $nameFile);
            if (str_contains($nameFile, 'windows')) {
                $nameFile = str_replace('windows', '', $nameFile);

                if ($nameFile == $nameSelectedOs) {
                    $xml = json_decode(json_encode(simplexml_load_file(request()->file("xmlFile"))));
                    $checker = true;
                }
                else {
                    $contain['message'] = 'Le nom du fichier ne correspond pas au sytème d\'exploitation que vous avez selectionné';
                }
            } else {
                $contain['message'] = 'Le nom de votre fichier doit contenir le mot "windows" afin d\'être valide';
            }
        }else {
            $contain['message'] = 'Vous devez importer un fichier xml !';
        }

        if ($checker){
            foreach ($xml as $YourKeys) {
                foreach ($YourKeys as $keys) {
                    foreach ($keys as $key) {
                        $importKeys[$i]['key'] = $key;
                        $importKeys[$i]['product_id'] = \request('product_id');
                        $importKeys[$i]['used'] = false;
                        $importKeys[$i]['msdn'] = true;
                        $importKeys[$i]['key_volume'] = false;
                        $i+=1;
                    }
                }
            }

            foreach ($importKeys as $key) {


                $validator = Validator::make($key, [
                    'key'=>'required|max:255|unique:keys',
                    'product_id'=>'required',
                    'used'=>'required',
                    'msdn'=>'required|boolean',
                    'key_volume' =>'required|boolean'
                ]);
                if ($validator->fails()) {

                    return redirect('import')
                        ->withErrors($validator)
                        ->withInput();
                }
                Key::create($key);
            }
            $contain['type'] = 'success';
            $contain['message'] = 'Les clés ont bien été importé';
        }else{
            $contain['type'] = 'error';
        }


        return redirect()->to('import')->with('contain', $contain);
    }

    public function edit(Key $key){
        $products = Product::get();
        return view('import.edit', compact('key','products'));
    }

    public function update(Key $key){
        $attKey = request()->validate([
            'key'=>'required|max:255',
            'product_id'=>'required',
            'msdn'=>'required|boolean'
        ]);

        if (request('name_sta') || request('mac') || request('email')){
            $attUsedKey = request()->validate([
                'mac'=>'required|max:255',
                'email'=>'required|email|max:255',
                'name_sta'=>'required'
            ]);
            $attKey['used'] = true;
            $attUsedKey['key_id'] = $key->id;
            $key->used_key->update($attUsedKey);
        }else{
            $attKey['used'] = false;
            if ($key->used_key){
                $key->used_key->delete();
            }
        }
        $key->update($attKey);

        $message = "La clé numero ".$key->id." à bien été modifiée";
        if ($key->used){
            $message.= " et est utilisé par ".$key->used_key->email;
        }
        $message .= " !!";

        return redirect()->to('import')->with('message', $message);


    }

    public function destroy(Key $key){
        $id = $key->id;
        $key->delete();

        $message = "La clé numero ". $id." à été bien été supprimée !";
        return redirect()->to('import')->with('message', $message);
    }


}
