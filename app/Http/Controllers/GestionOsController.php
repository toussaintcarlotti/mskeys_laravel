<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class GestionOsController extends Controller
{


    public function index(){
        $products = Product::get();
        return view("gestionOS.index", compact('products'));
    }

    public function create(){
        return view("gestionOS.create");
    }

    public function store(){
        $attribute = request()->validate([
            'name'=>'required|max:255'
        ]);
        $product = Product::create($attribute);

        $contain['type'] = "success";
        $contain['message'] = "Le système d'exploitation ". $product->name." à été bien été créé !";

        return redirect()->to('gestOS')->with('contain', $contain);
    }

    public function edit(Product $product){
        return view("gestionOS.edit", compact('product'));
    }
    public function update(Product $product){
        $attribute = request()->validate([
            'name'=>'required|max:255'
        ]);
        $product->update($attribute);

        $contain['type'] = "success";
        $contain['message'] = "Le système d'exploitation ". $product->name." à été bien été modifié !";

        return redirect()->to('gestOS')->with('contain', $contain);
    }

    public function destroy(Product $product){
        $product->delete();

        $contain['message'] = "Le système d'exploitation ". $product->name." à été bien été supprimé !";
        $contain['type'] = "success";
        return redirect()->to('gestOS')->with('contain', $contain);
    }
}
