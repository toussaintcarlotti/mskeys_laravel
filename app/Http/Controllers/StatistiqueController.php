<?php

namespace App\Http\Controllers;

use App\Models\Key;
use App\Models\Product;
use App\Models\Used_key;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StatistiqueController extends Controller
{
    public function index(){
        $resultProductKeys = null;

        $totalKeys = Key::get()->count();

        //clés par utilisateur
        $usersKeys = Used_key::get()->groupBy('email');

        //clés par OS
        $productsKeys = Product::withCount('keys')->get();

        //clés dispoonible par OS
        $dispoProductsKeys = Product::join('keys','keys.product_id','products.id')->where('used', false)->get()->groupBy('product_id');

        //clés utilisées par OS
        $usedProductsKeys = Product::join('keys','keys.product_id','products.id')->where('used', true)->get()->groupBy('product_id');

        $i = 1;
        foreach ($productsKeys as $productsKey) {
            if (isset($dispoProductsKeys[$productsKey->id])) {
                if ($dispoProductsKeys[$productsKey->id]->first()->product_id == $productsKey->id) {
                    $productsKey['dispo_keys_count'] = $dispoProductsKeys[$productsKey->id]->count();
                }
                if (isset($usedProductsKeys[$productsKey->id])) {
                    $productsKey['used_keys_count'] = $usedProductsKeys[$productsKey->id]->count();
                }
            }
            $resultProductKeys[$i] = $productsKey;
            $i += 1;

        }

        return view('statistique.index',compact('usersKeys','resultProductKeys','totalKeys'));
    }

}
