<?php

namespace App\Observers;

use App\Models\Key;

class KeyObserver
{
    /**
     * Handle the Key "created" event.
     *
     * @param  \App\Models\Key  $key
     * @return void
     */
    public function created(Key $key)
    {
        //
    }

    /**
     * Handle the Key "updated" event.
     *
     * @param  \App\Models\Key  $key
     * @return void
     */
    public function updated(Key $key)
    {
        //
    }

    /**
     * Handle the Key "deleted" event.
     *
     * @param  \App\Models\Key  $key
     * @return void
     */
    public function deleted(Key $key)
    {
        $key->used_key()->delete();
    }

    /**
     * Handle the Key "restored" event.
     *
     * @param  \App\Models\Key  $key
     * @return void
     */
    public function restored(Key $key)
    {
        //
    }

    /**
     * Handle the Key "force deleted" event.
     *
     * @param  \App\Models\Key  $key
     * @return void
     */
    public function forceDeleted(Key $key)
    {
        //
    }
}
