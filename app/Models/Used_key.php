<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Used_key extends Model
{
    use HasFactory;
    protected $fillable = [
        'mac',
        'email',
        'name_sta',
        'key_id'
    ];

    public function key()
    {
        return $this->belongsTo(Key::class);
    }
}
