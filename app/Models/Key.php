<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Key extends Model
{
    use HasFactory;
    protected $fillable = [
        'key',
        'product_id',
        'used',
        'msdn',
        'key_volume'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function used_key()
    {
        return $this->hasOne(Used_key::class);
    }
}
