<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

## Information utile (après installation)

- Utilisateur de base (a supprimer):

Email/login : test@test

Password : password

## Installation
Avant tout, vous devez installer toutes les dépendances nécessaires a l'application. Pour cela, lancez votre terminal 
et placez-vous à la racine de votre projet. Une fois que vous y êtes tapez les commandes suivantes :

    composer update && composer install

et

    npm install

ou

    yarn install
            

Ensuite configurer votre .env afin de vous connecter à votre base de données. Un fichier .env.example est présent 
à la racine de votre projet, vous pouvez simplement retirer le .example du nom de ce fichier et remplacer les 
informations qui vous concerne (DB_HOST, DB_PORT, DB_DATABASE, DB_USERNAME, DB_PASSWORD) a l'interieur de celui-ci.

Une fois cela réalisé plusieurs solutions s'offre à vous afin de gérer votre base de données :

- Utiliser Laravel(le plus simple et rapide) : 

Si vous voulez utiliser les fonctionnalités de laravel afin de configurer 
parfaitement votre base voici ce que vous pouvez faire (les ligne de commande 
suivante sont à entrer dans un terminal lorsque vous vous situer a la racine de 
votre application) :
1) base de données complète

La commande permettant de peupler totalement la base de données avec des information
 de test (faker est utilisé):

        php artisan migrate:fresh --seed
Après avoir taper cette commande votre application est totalement opérationnelle.

2) base de données vide

La commande permettant de créer toutes les tables nécessaires au bon 
fonctionnement de l'application (les tables seront totalement vide) :

        php artisan migrate:fresh

Une fois que cela est fait vous pouvez peupler votre base de données comme vous 
le souhaiter grace aux seeder et factory de Laravel.

1)Ajouter un utilisateur de test :

        php artisan db:seed --class=UserSeeder

2)Ajouter des OS (4) :

        php artisan db:seed --class=ProductSeeder

3)Ajouter des clés (500) :

        php artisan db:seed --class=KeySeeder

Attention ! Si vous ne lancer pas la commande numero 2 les 4 OS seront tout de même créer
automatiquement afin de garder un base cohérente. Si vous decider de créer manuelement vos OS
et de lancer cette commande sachez qu'il faut créer au moins 4 OS pour que votre base 
reste cohérente.


- Utiliser la base de test fourni :

Pour cela il vous suffit de récupérer le fichier sql présent a la racine de 
votre application (bdd_mskeys.sql) et de l'importer dans votre base de données.



