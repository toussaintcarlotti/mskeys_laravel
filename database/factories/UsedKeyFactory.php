<?php

namespace Database\Factories;

use App\Models\Key;
use App\Models\Model;
use Illuminate\Database\Eloquent\Factories\Factory;

class UsedKeyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Model::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'mac' => $this->faker->macAddress,
            'email' => $this->faker->email,
            'name_sta' => $this->faker->domainName,

        ];
    }
}
