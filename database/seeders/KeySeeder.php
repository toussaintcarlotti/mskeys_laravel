<?php

namespace Database\Seeders;

use App\Models\Key;
use App\Models\Product;
use Illuminate\Database\Seeder;

class KeySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (! Product::get()->first()){
            Product::create(['name'=>'Microsoft Windows xp']);
            Product::create(['name'=>'Microsoft Windows 7']);
            Product::create(['name'=>'Microsoft Windows 8']);
            Product::create(['name'=>'Microsoft Windows 10']);
        }
        Key::factory(500)->create();
    }
}
