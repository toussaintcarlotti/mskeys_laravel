<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create(['name'=>'Microsoft Windows xp']);
        Product::create(['name'=>'Microsoft Windows 7']);
        Product::create(['name'=>'Microsoft Windows 8']);
        Product::create(['name'=>'Microsoft Windows 10']);
        Product::create(['name'=>'Microsoft Windows 10 Éducation']);
    }
}
