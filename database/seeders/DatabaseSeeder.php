<?php

namespace Database\Seeders;

use App\Models\Key;
use App\Models\Product;
use App\Models\Used_key;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(1)->create();
        Product::create(['name'=>'Microsoft Windows xp']);
        Product::create(['name'=>'Microsoft Windows 7']);
        Product::create(['name'=>'Microsoft Windows 8']);
        Product::create(['name'=>'Microsoft Windows 10']);
        Key::factory(500)->create();


    }
}
