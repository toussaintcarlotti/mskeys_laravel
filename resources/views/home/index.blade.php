<x-app-layout>
    @if(session('contain'))
        <x-notification :contain="session('contain')"/>
    @endif
    @if($products->first())
        <div class="md:mx-20 lg:mx-32 mx-2 text-center mb-10 sm:mb-0">
            Afin d'obtenir une clé d'activation Windows,
            veuillez choisir votre Système d'Exploitation ainsi qu'une des clés de license proposées
            ci-dessous.
        </div>
        <div class="w-48 m-auto my-3 border-t border-gray-300 rounded-md text-center">
            <form >
                <x-jet-dropdown width="48">
                    <x-slot name="trigger">
                    <span class="inline-flex rounded-md">
                        <button type="button" class="inline-flex items-center px-3 py-2 border border-transparent
                        leading-4 font-medium rounded-md text-black bg-white hover:text-gray-700 focus:outline-none transition">
                           Choisissez votre OS
                            <svg class="ml-2 -mr-0.5 h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                            </svg>
                        </button>
                    </span>
                    </x-slot>

                    <x-slot name="content">
                        @foreach($products as $prod)
                            <button type="submit" value="{{ $prod->id }}" name="product_id" class="w-full">
                            <x-jet-dropdown-link>
                                {{ $prod->name }}
                            </x-jet-dropdown-link>
                            </button>
                        @endforeach
                    </x-slot>
                </x-jet-dropdown>

            </form>
        </div>
        @if($validate)
            @if($keys->first())
                <div class="container px-1 md:px-10 pt-6 pb-10 shadow-xl bg-gray-100 rounded-2xl w-full md:w-3/4  m-auto text-center border border-gray-200">
                    <div class="rounded-lg">
                        <h1 class="sticky top-0 font-bold mb-4 py-2 bg-gray-800 rounded-md m-auto md:w-2/3 lg:w-1/3 rounded text-indigo-100 text-center">{{ $product->name }}</h1>
                        <div class="flex flex-wrap justify-around">
                            @foreach($keys as $key)
                                <a class="px-3 mb-2 py-1 bg-white transition duration-500 hover:bg-gray-600 hover:text-white hover:border-transparent
                                    border border-black rounded-2xl w-full md:w-1/2 lg:w-1/3 mx-5" href="{{ url('/used_keys/create/'.$key->id) }}">{{ $key->key }}</a>
                            @endforeach
                        </div>
                    </div>
                </div>
            @else
                <div class="flex flex-col md:flex-row">
                    <div>
                        <h1 class="text-lg mt-2">Aucune clés n'est disponible pour ce système d'exploitation veuillez en ajouter</h1>
                    </div>
                    <div class="md:ml-auto md:mx-0 mx-auto">
                        <a href="{{ url("keys/create") }}" class="mr-2">
                            <x-jet-secondary-button >
                                Ajouter une clé
                                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                                </svg>
                            </x-jet-secondary-button>
                        </a>
                    </div>
                </div>
            @endif
        @endif
    @else
        <div class="flex flex-col md:flex-row">
            <div>
                <h1 class="text-lg mt-2">Afin d'obtenir une nouvelle clé veuillez d'abord enregistrer un système d'exploitation</h1>
            </div>
            <div class="md:ml-auto mx-auto">
                <a href="{{ url("gestOS/create") }}" class="mr-2">
                    <x-jet-secondary-button>
                       Enregistrer un OS
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                        </svg>
                    </x-jet-secondary-button>
                </a>
            </div>

        </div>

    @endif
</x-app-layout>



