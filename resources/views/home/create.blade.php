<x-app-layout>
    <div>
        <a href="{{ url('/?product_id='.$product->id) }}" >
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 19l-7-7m0 0l7-7m-7 7h18" />
            </svg>
        </a>
            <x-jet-validation-errors class="text-center" />
        <div class="container p-10 shadow rounded-2xl md:w-2/3 lg:w-1/2 w-full m-auto text-center">


            <h1 class="font-bold">Veulliez completer les informations</h1>
            <form method="POST" action="{{ url('used_keys/create') }}" class=" py-4">
                @csrf
                <input type="hidden" value="{{ $key->id }}" name="key_id">
                <div>
                    <x-jet-label for="key" value="{{ __('Clé :') }}" />
                    <x-jet-input id="key" class="block mt-1 w-full bg-gray-200 text-center" disabled type="text" name="name" value="{{ $key->key }}" required autofocus autocomplete="key" />
                </div>

                <div class="mt-4">
                    <x-jet-label for="os" value="{{ __('OS :') }}" />
                    <x-jet-input id="os" class="block mt-1 w-full bg-gray-200 text-center" disabled type="text" name="os" value="{{ $product->name }}" required />
                </div>

                <div class="mt-4">
                    <x-jet-label for="name_sta" value="{{ __('Nom STA :') }}" />
                    <x-jet-input id="name_sta" class="block mt-1 w-full text-center" type="text" name="name_sta" required autocomplete="none" />
                </div>

                <div class="mt-4">
                    <x-jet-label for="mac" value="{{ __('Adresse MAC :') }}" />
                    <x-jet-input id="mac" class="block mt-1 w-full text-center" type="text" name="mac" required autocomplete="none" />
                </div>
                <div class="mt-4">
                    <x-jet-label for="email" value="{{ __('Email :') }}" />
                    <x-jet-input id="email" class="block mt-1 w-full text-center" type="email" name="email" required autocomplete="email" />
                </div>

                <x-jet-button type="submit" class="mt-3">Valider</x-jet-button>
            </form>
        </div>
    </div>
</x-app-layout>
