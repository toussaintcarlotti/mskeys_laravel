<x-app-layout>

<div class="flex flex-row">
    <a href="{{ url('gestOS') }}" >
        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 19l-7-7m0 0l7-7m-7 7h18" />
        </svg>
    </a>

    <form action="{{ url("gestOS/delete/".$product->id) }}" method="post" class="ml-auto">
        @csrf
        @method("delete")
        <x-jet-danger-button type="submit" class="mt-3 mr-2">Supprimer</x-jet-danger-button>
    </form>
</div>


    <x-jet-validation-errors class="text-center" />
    <div class="container p-10 shadow rounded-2xl md:w-2/3 lg:w-1/2 w-full m-auto text-center">
        <h1 class="font-bold">Veulliez completer les informations</h1>
        <form method="POST" action="{{ url('gestOS/edit/'.$product->id) }}" class=" py-4">
            @csrf
            <div  class="mt-4">
                <x-jet-label for="name" value="{{ __('Nom :') }}" class="text-lg"/>
                <x-jet-input id="name" class="block mt-1 w-full text-center" type="text" name="name" value="{{ $product->name }}" required autofocus autocomplete="none" />
            </div>
            <x-jet-button type="submit" class="mt-3">Modifier</x-jet-button>
        </form>


    </div>

</x-app-layout>
