<x-app-layout>
    @if(session('contain'))
        <x-notification :contain="session('contain')"/>
    @endif
    <div class="flex flex-col md:flex-row">
        <div class="md:ml-auto md:mx-0 mx-auto">
            <a href="{{ url("gestOS/create") }}" class="mr-2">
            <x-jet-secondary-button >
                Enregistrer un OS
                <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                </svg>
            </x-jet-secondary-button>
            </a>
        </div>
    </div>

    @if($products->first())
        <div >
            <table class="md:w-1/2 md:mx-auto w-full mx-2 my-3 mt-5 border-collapse table-auto bg-white bg-gray-50">
                <thead>
                <tr>
                    <th class="bg-gray-200 sticky rounded-tl-2xl px-6 py-2 text-gray-600 font-bold tracking-wider uppercase text-xs">Id</th>
                    <th class="bg-gray-200 sticky px-6 py-2 rounded-tr-2xl text-gray-600 font-bold tracking-wider uppercase text-xs">Nom</th>

                </tr>
                </thead>
                <tbody>
                @foreach($products as $product)

                    <tr class="text-center hover:bg-gray-800 hover:text-white transition duration-500 cursor-pointer" onclick="fhref({{ $product->id }})">
                        <td class="px-6 py-2 ">{{ $product->id }}</td>
                        <td class="px-6 py-2 ">{{ $product->name }}</td>
                    </tr>

                @endforeach
                </tbody>
            </table>
    @endif
</x-app-layout>

<script >
    function fhref(productId)
    {
        window.location = 'gestOS/edit/' + productId;
    }
</script>

<style>




</style>
