<div class="notification fixed py-4 lg:px-4 top-14 right-0">
    <div class="p-2 items-center leading-none rounded-l-full w-full flex lg:inline-flex
    @if($contain['type'] == "success")
        bg-green-800 text-green-100
    @endif
    @if($contain['type'] == "error")
        bg-red-800 text-red-100
    @endif
    @if($contain['type'] == "warning")
        bg-yellow-800 text-yellow-100
    @endif
        " role="alert">
        @if($contain['type'] == "success")
            <span class="flex rounded-full bg-green-500 uppercase px-2 pb-1 pt-2 text-xs font-bold mr-3 w-28 text-center">Succès !</span>
        @endif
        @if($contain['type'] == "error")
            <span class="flex rounded-full bg-red-500 uppercase px-2 pb-1 pt-2 text-xs font-bold mr-3 w-28 text-center">Erreur !</span>
        @endif
        @if($contain['type'] == "warning")
            <span class="flex rounded-full bg-yellow-500 uppercase px-2 pb-1 pt-2 text-xs font-bold mr-3 w-28 text-center">Attention !</span>
        @endif
        <span class="font-semibold mr-2 text-left flex-auto">{{ $contain['message'] }}</span>
    </div>
</div>

<style>
    @keyframes myAnimation{
        0%{
            opacity: 0.5;
            transform: rotateX(90deg);
        }
        10%{
            opacity: 0.8;
            transform: rotateX(0deg);
        }90%{
            opacity: 0.8;
            transform: rotateX(0deg);
        }
        100%{
            display: none;
            opacity: 0;
            transform: rotateX(90deg);
        }
    }

    .notification{
        width: 33%;
        animation-name: myAnimation;
        animation-duration: 8s;
        animation-fill-mode: forwards;
    }
</style>
