<x-app-layout>
    <a href="{{ url('import') }}" >
        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 19l-7-7m0 0l7-7m-7 7h18" />
        </svg>
    </a>
    <div class="xl:flex xl:flex-row">
        <div class="xl:w-1/2 md:w-3/5 md:mx-auto w-full">
            <img src="
            @if( $key->used_key )
            {{ url('img/usedKeyShow.gif') }}
            @else
            {{ url('img/keyShow.gif') }}
            @endif "
                 class=" transform rotate-90 my-20 xl:mb-10"
                 alt="key_image">
        </div>

        <div class="border border-gray-200 shadow-lg w-full xl:w-1/2 mx-auto p-2 pt-10 md:p-10 rounded-md md:text-base text-center">
            <h1 class="text-center text-2xl mb-5 -mt-3">Information clé </h1>

            <div class="flex flex-col sm:flex-row mb-10">
                <x-jet-label class="text-xl mr-2 font-bold" value="{{ __('Clé : ') }}" />
                <a class="text-lg"> {{ $key->key }}</a>
            </div>
            <div class="flex flex-col sm:flex-row mb-10">
                <x-jet-label class="text-xl mr-2 font-bold" value="Système d'exploitation : " />
                <a class="text-lg">{{ $key->product->name }}</a>
            </div>
            <div class="flex flex-col sm:flex-row mb-10">
                <x-jet-label class="text-xl mr-2 font-bold" value="Msdn : " />
                    @if($key->msdn == 0)
                        <a class="text-lg text-red-500">Non</a>
                    @else
                        <a class="text-lg text-blue-500">Oui</a>
                    @endif
            </div>



            @if( $key->used_key )
                <div class="flex flex-col sm:flex-row mb-10">
                    <x-jet-label class="text-xl mr-2 font-bold" value="Disponible : " />
                    <a class="text-lg text-red-500">Non</a>
                </div>
                <h1 class="text-center text-2xl border-t border-gray-200 mb-5">Information utilisateur </h1>

                <div class="flex flex-col sm:flex-row mb-10">
                    <x-jet-label class="text-xl mr-2 font-bold" value="{{ __('Utilisée par : ') }}" />
                    <a class="text-lg"> {{ $key->used_key->email }}</a>
                </div>

                <div class="flex flex-col sm:flex-row mb-10">
                    <x-jet-label class="text-xl mr-2 font-bold" value="{{ __('Adresse MAC : ') }}" />
                    <a class="text-lg"> {{ $key->used_key->mac }}</a>
                </div>

                <div class="flex flex-col sm:flex-row mb-10">
                    <x-jet-label class="text-xl mr-2 font-bold" value="{{ __('Nom STA : ') }}" />
                    <a class="text-lg"> {{ $key->used_key->name_sta }}</a>
                </div>
            @else
                <div class="flex flex-col sm:flex-row mb-10">
                    <x-jet-label class="text-xl mr-2 font-bold" value="Diponible : " />
                    <a class="text-lg text-green-500">Oui</a>
                </div>
            @endif

        </div>
    </div>
    <div class="text-center flex flex-row justify-center mt-10">
        <form action="{{ url("keys/delete/".$key->id) }}" method="post">
            @csrf
            @method("delete")
            <x-jet-danger-button type="submit" class="mt-3 mr-2">Supprimer</x-jet-danger-button>
        </form>
            <x-jet-secondary-button type="submit" class="mt-3 bg-yellow-300"><a href="{{ url("keys/edit/".$key->id) }}">Modifier</a></x-jet-secondary-button>


    </div>

</x-app-layout>
