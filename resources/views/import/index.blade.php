<x-app-layout>
    <x-jet-validation-errors class="text-center" />
    @if(session('contain'))
        <x-notification :contain="session('contain')"/>
    @endif


    @if($products->first())
        <div class="flex flex-col md:flex-row">
            <div class="w-48 md:m-1 mx-auto mb-5">
                <form>
                    <x-jet-dropdown width="48">
                        <x-slot name="trigger">
                                <span class="inline-flex rounded-md">
                                    <button type="button" class="inline-flex items-center px-3 border border-transparent
                                    leading-4 rounded-md text-black bg-white hover:text-gray-700 focus:outline-none transition">
                                        Choisissez votre OS
                                        <svg class="ml-2 -mr-0.5 h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                            <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                                        </svg>
                                    </button>
                                </span>
                        </x-slot>
                        <x-slot name="content" >
                            @if($product->name)
                                <button type="submit" class="w-full">
                                    <x-jet-dropdown-link>
                                        Tous les OS
                                    </x-jet-dropdown-link>
                                </button>
                            @endif
                            @foreach($products as $prod)
                                <button type="submit" value="{{ $prod->id }}" name="product_id" class="w-full">
                                    <x-jet-dropdown-link>
                                        {{ $prod->name }}
                                    </x-jet-dropdown-link>
                                </button>
                            @endforeach
                        </x-slot>
                    </x-jet-dropdown>
                </form>
            </div>
            <div class="md:ml-auto md:mx-0 mx-auto">
                <a href="{{ url('keys/create') }}" class="mr-2">
                    <x-jet-secondary-button>
                        Enregistrer une clé
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                        </svg>
                    </x-jet-secondary-button>
                </a>
            </div>
        </div>
            @if($product->name)
                <div class="py-1 @if($product->name) sticky top-0 @endif bg-white">
                    <h1 class="xl:w-1/3 lg:w-1/2 w-10/12 font-bold mb-4 py-2 bg-gray-800 rounded-md m-auto rounded text-white text-center ">
                        <label class="text-indigo-100 capitalize ">{{ $product->name }}</label>
                    </h1>
                </div>

            @endif

    @else
        <div class="flex flex-col md:flex-row">
            <div>
                <h1 class="text-lg mt-2">Afin d'obtenir une nouvelle clé veuillez d'abord enregistrer un système d'exploitation</h1>
            </div>
            <div class="md:ml-auto mx-auto">
                <a href="{{ url("gestOS/create") }}" class="mr-2">
                    <x-jet-secondary-button>
                        Enregistrer un OS
                        <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 6v6m0 0v6m0-6h6m-6 0H6" />
                        </svg>
                    </x-jet-secondary-button>
                </a>
            </div>
        </div>
    @endif

    @if($product->name)
        <form action="{{ url('keys/createXml') }}" method="post" enctype="multipart/form-data" class="flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-gray-100">
            @csrf
            <input  type="hidden" value="{{ $product }}" name="os">
            <input type="hidden" value="{{ $product->id }}" name="product_id">

            <x-jet-label class="text-lg">Importer un fichier XML pour cet OS : </x-jet-label>
            <input type="file" name="xmlFile" required>

            <x-jet-secondary-button type="submit" >
                Importer
            </x-jet-secondary-button>

        </form>
    @endif

    <div class="border-t border-gray-200 mt-2 mb-6"></div>
    <div class="w-full bg-white py-2 h-12">
        @if(! $keys->first())
            @if($product->name)
                <h1 class="xl:w-1/3 lg:w-1/2 w-10/12 font-bold mb-4 py-2 bg-gray-800 rounded-md rounded text-white text-center mx-auto">
                    Aucune clé n'est enregistrée pour cet OS
                </h1>
            @else
                <h1 class="xl:w-1/3 lg:w-1/2 w-10/12 font-bold mb-4 py-2 bg-gray-800 rounded-md rounded text-white text-center mx-auto">
                    Aucune clé n'est enregistrée
                </h1>
            @endif
        @endif

    </div>
    @if($keys->first())
        <table class="container mx-auto border-collapse table-auto bg-white w-full bg-gray-50 h-1/2 rounded-t-2xl">
            <thead>
            <tr>
                <th class="bg-gray-200 sticky @if($product->name)top-12 @else top-0 @endif rounded-tl-2xl px-6 py-2 text-gray-600 font-bold tracking-wider uppercase text-xs">Id</th>
                <th class="collapse sm:rounded-none rounded-tl-2xl bg-gray-200 sticky @if($product->name)top-12 @else top-0 @endif px-6 py-2 text-gray-600 font-bold tracking-wider uppercase text-xs">Key</th>
                <th class="collapse2 bg-gray-200 sticky @if($product->name) top-12 @else top-0 @endif px-6 py-2 text-gray-600 font-bold tracking-wider uppercase text-xs">Disponible</th>
                <th class="collapse bg-gray-200 sticky @if($product->name)top-12 @else top-0 @endif px-6 py-2 text-gray-600 font-bold tracking-wider uppercase text-xs">Msdn</th>
                <th class="collapse bg-gray-200 sticky @if($product->name)top-12 rounded-tr-2xl @else top-0 @endif px-6 py-2 text-gray-600 font-bold tracking-wider uppercase text-xs">Clé de volume</th>
                @if(!$product->name)
                    <th class="bg-gray-200 sticky top-0 px-6 rounded-tr-2xl py-2 text-gray-600 font-bold tracking-wider uppercase text-xs blue">OS</th>
                @endif
            </tr>
            </thead>
            <tbody>
            @foreach($keys as $key)

                <tr class="text-center hover:bg-gray-800 hover:text-white transition duration-500 cursor-pointer" onclick="fhref({{ $key->id }})">
                    <td class="px-6 py-2 ">{{ $key->id }}</td>
                    <td class="collapse px-6 py-2 ">{{ $key->key }}</td>
                    @if($key->used == 0)
                        <td class="collapse2 px-6 py-2 text-green-500">Oui</td>
                    @else
                        <td class="collapse2 px-6 py-2 text-red-500">Non</td>
                    @endif

                    @if($key->msdn == 0)
                        <td class="collapse px-6 py-2 text-red-500">Non</td>
                    @else
                        <td class="collapse px-6 py-2 text-blue-500">Oui</td>
                    @endif

                    @if($key->key_volume == 0)
                        <td class="collapse px-6 py-2 text-red-500">Non</td>
                    @else
                        <td class="collapse px-6 py-2 text-blue-500">Oui</td>
                    @endif

                    @if(!$product->name)
                        <td class="px-6 py-2 ">{{ $key->product->name }}</td>
                    @endif
                </tr>

            @endforeach
            </tbody>
        </table>
    @endif
</x-app-layout>

<style>
    @media (max-width: 768px) {
        .collapse{
            display: none;
        }
    }
    @media (max-width: 600px) {
        .collapse2{
            display: none;
        }

    }
</style>

<script >
    function fhref(keyId)
    {
        window.location = 'keys/show/' + keyId;
    }
</script>
