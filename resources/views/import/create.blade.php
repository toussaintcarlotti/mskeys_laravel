<x-app-layout>
    <div>
        <a href="{{ url('import') }}" >
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 19l-7-7m0 0l7-7m-7 7h18" />
            </svg>
        </a>
        <x-jet-validation-errors class="text-center" />
        <div class="container p-10 shadow rounded-2xl md:w-2/3 lg:w-1/2 w-full m-auto text-center">
            <h1 class="font-bold">Veulliez completer les informations</h1>
            <form method="POST" action="{{ url('keys/create') }}" class=" py-4">
                @csrf
                <div>
                    <x-jet-label for="clé" value="{{ __('Clé :') }}" class="text-lg"/>
                    <x-jet-input id="clé" class="block mt-1 w-full text-center" type="text" name="key"  required autofocus autocomplete="key" />
                </div>

                <div class="mt-4">
                    <x-jet-label for="os" value="{{ __('OS :') }}" class="text-lg" />
                    @if($products->first())
                    <select style="text-align-last: center"
                            class=" w-full m-auto my-3 border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm"
                            name="product_id"
                    >
                        @foreach($products as $prod)
                            <option value="{{ $prod->id }}" >
                                {{ $prod->name }}
                            </option>
                        @endforeach

                    </select>
                    @else
                        <x-jet-input class="block mt-1 w-full text-center placeholder-red-500"
                                     type="text" disabled
                                     placeholder="Aucun OS n'est enregistré !"/>
                    @endif

                </div>

                <div class="mt-4">
                    <x-jet-label for="msdn" value="{{ __('MSDN :') }}" class="text-lg" />
                    <div class="mt-2" id="msdn">
                        <input type="radio" id="oui" name="msdn" value="1"
                               checked>
                        <label for="oui" class="mr-3">Oui</label>

                        <input type="radio" id="non" name="msdn" value="0">
                        <label for="non">Non</label>
                    </div>
                </div>

                <div class="mt-4">
                    <x-jet-label for="key_volume" value="{{ __('Clé de volume :') }}" class="text-lg" />
                    <div class="mt-2" id="key_volume">
                        <input type="radio" id="oui" name="key_volume" value="1">
                        <label for="oui" class="mr-3">Oui</label>

                        <input type="radio" id="non" name="key_volume" value="0" checked >
                        <label for="non">Non</label>
                    </div>
                </div>

                <div class="mt-4">
                    <div>
                        <button type="button" class="inline-flex items-center px-3 py-2 border border-transparent text-xl
                            leading-4 font-medium rounded-md text-black bg-white hover:text-gray-700 focus:outline-none transition hasSubMenu">
                            <x-jet-label value="{{ __(' Utilisateur ?') }}" class="text-lg cursor-pointer" />

                            <svg class="ml-2 -mr-0.5 h-4 w-4" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor">
                                <path fill-rule="evenodd" d="M5.293 7.293a1 1 0 011.414 0L10 10.586l3.293-3.293a1 1 0 111.414 1.414l-4 4a1 1 0 01-1.414 0l-4-4a1 1 0 010-1.414z" clip-rule="evenodd" />
                            </svg>
                        </button>
                        <ul class="text-sm subMenu hidden">
                            <div class="mt-4">
                                <x-jet-label for="name_sta" value="{{ __('Nom STA :') }}" />
                                <x-jet-input id="name_sta" class="block mt-1 w-full text-center" type="text" name="name_sta" autocomplete="none" />
                            </div>

                            <div class="mt-4">
                                <x-jet-label for="mac" value="{{ __('Adresse MAC :') }}" />
                                <x-jet-input id="mac" class="block mt-1 w-full text-center" type="text" name="mac" autocomplete="none" />
                            </div>

                            <div class="mt-4">
                                <x-jet-label for="email" value="{{ __('Email :') }}" />
                                <x-jet-input id="MAC" class="block mt-1 w-full text-center optiona" type="text" name="email" autocomplete="email" />
                            </div>
                        </ul>
                    </div>

                </div>


                <x-jet-button type="submit" class="mt-3">Valider</x-jet-button>
            </form>
        </div>
    </div>
</x-app-layout>

<script>
    function toggleMenu (btn) {
        const el = btn.parentElement.querySelector('.subMenu')
        el.classList.toggle('hidden')
    }
    const btn = document.querySelector('.hasSubMenu')
    btn.addEventListener('click', function(){
        toggleMenu(btn)
    })
</script>
