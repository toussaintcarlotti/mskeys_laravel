<x-app-layout>
    <div>
        <a href="{{ url('keys/show/'.$key->id) }}" >
            <svg xmlns="http://www.w3.org/2000/svg" class="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 19l-7-7m0 0l7-7m-7 7h18" />
            </svg>
        </a>
        <x-jet-validation-errors class="text-center" />
        <div class="container p-10 shadow rounded-2xl md:w-2/3 lg:w-1/2 w-full m-auto text-center">
            <h1 class="font-bold">Veulliez completer les informations</h1>
            <form id="form" method="POST" action="{{ url('keys/edit/'.$key->id) }}" class=" py-4">
                @csrf
                <div  class="mt-4">
                    <x-jet-label for="key" value="{{ __('Clé :') }}" class="text-lg"/>
                    <x-jet-input id="key" class="block mt-1 w-full text-center" type="text" name="key" value="{{ $key->key }}"  required autofocus autocomplete="key" />
                </div>

                <div class="mt-4">
                    <x-jet-label for="os" value="{{ __('OS :') }}" class="text-lg" />
                    <select style="text-align-last: center"
                            class=" w-full m-auto my-3 border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 rounded-md shadow-sm"
                            name="product_id"
                    >
                        @foreach($products as $prod)
                            <option value="{{ $prod->id }}"
                                    @if($key->product->id == $prod->id)
                                        selected
                                    @endif >
                                {{ $prod->name }}
                            </option>
                        @endforeach

                    </select>

                    <div class="mt-4">
                        <x-jet-label for="msdn" value="{{ __('MSDN :') }}" class="text-lg" />
                        <div class="mt-2" id="msdn">
                            <input type="radio" id="oui" name="msdn" value="1"
                                   @if($key->msd) checked @endif>
                            <label for="oui" class="mr-3">Oui</label>

                            <input type="radio" id="non" name="msdn" value="0"
                                   @if(! $key->msd) checked @endif>
                            <label for="non">Non</label>
                        </div>
                    </div>

                    <div class="mt-4">
                        <x-jet-label for="key_volume" value="{{ __('Clé de volume :') }}" class="text-lg" />
                        <div class="mt-2" id="key_volume">
                            <input type="radio" id="oui" name="key_volume" value="1"
                                   @if($key->key_volume) checked @endif>
                            <label for="oui" class="mr-3">Oui</label>

                            <input type="radio" id="non" name="key_volume" value="0"
                                   @if(! $key->key_volume) checked @endif>
                            <label for="non">Non</label>
                        </div>
                    </div>

                    @if($key->used)
                        <div class="border shadow bg-gray-100 p-5 -mx-5 mt-2 rounded-md">
                            <div class="text-right">
                                <x-jet-secondary-button onclick="vider()">Vider</x-jet-secondary-button>
                            </div>

                            <div  class="mt-4">
                                <x-jet-label for="email" value="{{ __('Email :') }}" class="text-lg"/>
                                <x-jet-input id="email" class="vide block mt-1 w-full text-center" type="text" name="email" value="{{ $key->used_key->email }}" autocomplete="key" />
                            </div>
                            <div  class="mt-4">
                                <x-jet-label for="mac" value="{{ __('Adresse MAC :') }}" class="text-lg"/>
                                <x-jet-input id="mac" class="vide block mt-1 w-full text-center" type="text" name="mac" value="{{ $key->used_key->mac }}" autocomplete="key" />
                            </div>
                            <div  class="mt-4">
                                <x-jet-label for="name_sta" value="{{ __('Nom STA :') }}" class="text-lg"/>
                                <x-jet-input id="name_sta" class="vide block mt-1 w-full text-center" type="text" name="name_sta" value="{{ $key->used_key->name_sta }}" autocomplete="key" />
                            </div>
                        </div>

                    @else
                        <div class="mt-4">
                            <a class="text-lg" href="{{ url('/used_keys/create/'.$key->id) }}">Ajouter un utilisateur ? </a>
                        </div>
                    @endif
                </div>

                <x-jet-button type="submit" class="mt-3">Valider</x-jet-button>
            </form>
        </div>
    </div>
</x-app-layout>

<script>
    function vider(){
        document.getElementById("email").value = "";
        document.getElementById("mac").value = "";
        document.getElementById("name_sta").value = "";
    }
</script>
