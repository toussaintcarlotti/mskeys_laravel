<x-app-layout>
    @if(session('contain'))
        <x-notification :contain="session('contain')"/>
    @endif
    @if($resultProductKeys)
    <h1 class="mb-6 -mt-1 mx-auto text-2xl text-center">Nombre de clés par OS :</h1>
    <div class="flex flex-wrap justify-evenly ">
        @foreach($resultProductKeys as $resultProductKey)
            @php
                if(isset($resultProductKey) && isset($totalKeys)){

                  $pourcentAllKeys = round((($resultProductKey->keys_count / $totalKeys)*100),1) ;


                  $pourcentDispoKeysCircle = round((($resultProductKey->dispo_keys_count / $totalKeys)*87),1) ;
                  $pourcentDispoKeys = round((($resultProductKey->dispo_keys_count / $totalKeys)*100),1) ;


                  $pourcentUsedKeys = round((($resultProductKey->used_keys_count / $totalKeys)*100),1) ;
                  $pourcentUsedKeysCircle = round((($resultProductKey->used_keys_count / $totalKeys)*75),1) ;
                  }
            @endphp

            <section class="mx-3">
                <h2 class="text-center">{{ $resultProductKey->name }}</h2>
                <svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" width="200" height="200" xmlns="http://www.w3.org/2000/svg">

                    <circle class="circle-chart__background" stroke="#efefef" stroke-width="2" fill="none"
                            cx="16.91549431" cy="16.91549431" r="15.91549431" />
                    @if($pourcentAllKeys)
                        <circle class="circle-chart__circle" stroke="purple" stroke-width="2" stroke-dasharray="{{ $pourcentAllKeys }},100"
                                stroke-linecap="round" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                    @endif

                    <circle class="circle-chart__background" stroke="#efefef" stroke-width="2" fill="none"
                            cx="16.91549431" cy="16.91549431" r="13.91549431" />
                    @if($pourcentDispoKeys)
                        <circle class="circle-chart__circle" stroke="green" stroke-width="2" stroke-dasharray="{{ $pourcentDispoKeysCircle }},100"
                                stroke-linecap="round" fill="none" cx="16.91549431" cy="16.91549431" r="13.91549431" />
                    @endif

                    <circle class="circle-chart__background" stroke="#efefef" stroke-width="2" fill="none"
                            cx="16.91549431" cy="16.91549431" r="12" />
                    @if($pourcentUsedKeys)
                        <circle class="circle-chart__circle" stroke="red" stroke-width="2" stroke-dasharray="{{ $pourcentUsedKeysCircle }},100"
                                stroke-linecap="round" fill="none" cx="16.91549431" cy="16.91549431" r="11.91549431" />
                    @endif



                    <g class="circle-chart__info" >
                        <text class="circle-chart__percent" x="16.91549431" y="12" alignment-baseline="central"
                              text-anchor="middle" font-size="4" fill="purple" font-weight="bold">
                            @if($resultProductKey->keys_count)
                                {{ $resultProductKey->keys_count }}
                            @else
                                0
                            @endif
                            <a font-size="2"> ({{ $pourcentAllKeys }}%)</a>
                        </text>


                        <text class="circle-chart__percent" x="16.91549431" y="17" alignment-baseline="central"
                              text-anchor="middle" font-size="4" color="green" fill="green" font-weight="bold">
                            @if( $resultProductKey->dispo_keys_count)
                                {{  $resultProductKey->dispo_keys_count }}
                            @else
                                0
                            @endif
                            <a font-size="2"> ({{ $pourcentDispoKeys }}%)</a>
                        </text>

                        <text class="circle-chart__percent text-purple-500" x="16.91549431" y="22" alignment-baseline="central"
                              text-anchor="middle" font-size="4" fill="red" font-weight="bold">

                            @if( $resultProductKey->used_keys_count)
                                {{  $resultProductKey->used_keys_count }}
                            @else
                                0
                            @endif
                            <a font-size="2"> ({{ $pourcentUsedKeys }}%)</a>
                        </text>

                    </g>
                </svg>
            </section>
        @endforeach
    </div>
    <div class="flex md:flex-row flex-col justify-center mt-5 border-b pb-5 border-gray-200 mb-5">
        <div class="flex inline-flex border border-gray-300 rounded-full px-4 py-1 md:mx-5 mx-auto justify-center mb-1" >
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="purple" class="bi bi-square-fill mt-1 mr-2" viewBox="0 0 16 16">
                <path d="M0 2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2z"/>
            </svg>
            Toutes les clés
        </div>
        <div class="flex inline-flex border border-gray-300 rounded-full px-4 py-1 md:mx-5 mx-auto justify-center mb-1">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="green" class="bi bi-square-fill mt-1 mr-2" viewBox="0 0 16 16">
                <path d="M0 2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2z"/>
            </svg>
            Clés disponibles
        </div>
        <div class="flex inline-flex border border-gray-300 rounded-full px-4 py-1 md:mx-5 mx-auto justify-center mb-1">
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="red" class="bi bi-square-fill mt-1 mr-2" viewBox="0 0 16 16">
                <path d="M0 2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2z"/>
            </svg>
            Clés utilisées
        </div>
    </div>
    @else
        <div class="text-center text-2xl"> Aucune statistique n'est disponible </div>

    @endif



    @if($usersKeys->first())
        <h1 class="mb-6 mx-auto text-2xl text-center">Nombre de clés par utilisateur :</h1>
        <div class="flex flex-wrap justify-evenly mb-5 pb-5">
            @foreach($usersKeys as $userKeys)
                @php
                    if(isset($userKeys) && isset($totalKeys)){
                      $pourcent = ($userKeys->count()/$totalKeys)*100 ;
                      }
                @endphp

                <section class="mx-3">
                    <h2 class="text-center">{{ $userKeys->first()->email }}</h2>
                    <svg class="circle-chart" viewbox="0 0 33.83098862 33.83098862" width="200" height="200" xmlns="http://www.w3.org/2000/svg">
                        <circle class="circle-chart__background" stroke="#efefef" stroke-width="2" fill="none"
                                cx="16.91549431" cy="16.91549431" r="15.91549431" />
                        <circle class="circle-chart__circle" stroke="black" stroke-width="2" stroke-dasharray="{{ $pourcent }},100"
                                stroke-linecap="round" fill="none" cx="16.91549431" cy="16.91549431" r="15.91549431" />
                        <g class="circle-chart__info">
                            <text class="circle-chart__percent" x="16.91549431" y="15.5" alignment-baseline="central"
                                  text-anchor="middle" font-size="8">{{ $userKeys->count() }}</text>
                            <text class="circle-chart__subline" x="16.91549431" y="20.5" alignment-baseline="central"
                                  text-anchor="middle" font-size="2"> {{ $pourcent }}% des clés</text>
                        </g>
                    </svg>

                </section>
            @endforeach
        </div>
    @endif
</x-app-layout>

<style>

    .circle-chart__circle {
        animation: circle-chart-fill 2s reverse; /* 1 */
        transform: rotate(-90deg); /* 2, 3 */
        transform-origin: center; /* 4 */
    }

    .circle-chart__circle--negative {
        transform: rotate(-90deg) scale(1,-1); /* 1, 2, 3 */
    }

    .circle-chart__info {
        animation: circle-chart-appear 2s forwards;
        opacity: 0;
        transform: translateY(0.3em);
    }

    @keyframes circle-chart-fill {
        to { stroke-dasharray: 0 100; }
    }

    @keyframes circle-chart-appear {
        to {
            opacity: 1;
            transform: translateY(0);
        }
    }


</style>
