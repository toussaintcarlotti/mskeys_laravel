<?php

use App\Http\Controllers\HomeController;
use App\Http\Controllers\ImportController;
use App\Http\Controllers\GestionOsController;
use App\Http\Controllers\StatistiqueController;
use Illuminate\Support\Facades\Route;
use Laravel\Fortify\Http\Controllers\RegisteredUserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['auth:sanctum', 'verified'])->group( function (){
    // Acceuil
    Route::get('/', [HomeController::class, 'index'])->name('home');
        // UsedKeys
        Route::get('used_keys/create/{key}', [HomeController::class, 'create'])->name('home');
        Route::post('used_keys/create', [HomeController::class, 'store']);


    // Importation
        // Keys
        Route::get('import', [ImportController::class, 'index'])->name('import');

            //créer
            Route::get('keys/create', [ImportController::class, 'create'])->name('import');
            Route::post('keys/create', [ImportController::class, 'store'])->name('import');
            //afficher
            Route::get('keys/show/{key}', [ImportController::class, 'show'])->name('import');
            //modifier
            Route::get('keys/edit/{key}', [ImportController::class, 'edit'])->name('import');
            Route::post('keys/edit/{key}', [ImportController::class, 'update'])->name('import');
            //supprimer
            Route::delete('keys/delete/{key}', [ImportController::class, 'destroy'])->name('import');

            //xml
            Route::post('keys/createXml', [ImportController::class, 'storeXml'])->name('import');

    // Gestion OS
    Route::get('gestOS', [GestionOsController::class, 'index'])->name('gestOS');

        Route::get('gestOS/create', [GestionOsController::class, 'create'])->name('gestOS');
        Route::post('gestOS/create', [GestionOsController::class, 'store'])->name('gestOS');
        Route::get('gestOS/edit/{product}', [GestionOsController::class, 'edit'])->name('gestOS');
        Route::post('gestOS/edit/{product}', [GestionOsController::class, 'update'])->name('gestOS');

        Route::delete('gestOS/delete/{product}', [GestionOsController::class, 'destroy'])->name('gestOS');

    // Statistiques
    Route::get('stats', [StatistiqueController::class, 'index'])->name('stats');

    //Register
    Route::get('account/create', [RegisteredUserController::class, 'create'])->name('newAccount');
    Route::post('account/create', [RegisteredUserController::class, 'store'])->name('newAccount');


});

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
    return view('dashboard');
})->name('dashboard');
